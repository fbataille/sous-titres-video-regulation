1
00:00:08,820 --> 00:00:11,320
So, that is easy to understand.

2
00:00:11,520 --> 00:00:17,280
Regulator fix some game rules that apply

3
00:00:17,440 --> 00:00:27,820
to charity organization and SME which are: Orange, SFR, Free, Bouygues, Vinci.

4
00:00:28,000 --> 00:00:31,200
It is mandatory for ARCEP to consul economic actors.

5
00:00:31,220 --> 00:00:37,228
When ARCEP is doing that, it consult expert actor, but on a very tiny market area.

6
00:00:37,820 --> 00:00:41,142
So, it ask an operator

7
00:00:41,170 --> 00:00:44,114
specialized in Internet for laywers,

8
00:00:44,257 --> 00:00:49,170
that will explain how its business will die if, on this market area

9
00:00:49,200 --> 00:00:51,120
we touch this or that parameter.

10
00:00:51,120 --> 00:00:53,511
These guys are experts

11
00:00:53,550 --> 00:00:55,617
but they have a very tiny scope view.

12
00:00:55,680 --> 00:00:57,680
They only look at their market segment.

13
00:00:58,000 --> 00:01:00,204
Largely because it is what they are interested in,

14
00:01:00,204 --> 00:01:01,571
which is save their business.

15
00:01:01,570 --> 00:01:05,626
On a capitalist economic market correctly ordered

16
00:01:05,620 --> 00:01:08,835
people want to make money. It is ... like that.

17
00:01:09,128 --> 00:01:10,740
And we arrive on this market

18
00:01:10,800 --> 00:01:14,080
being an operator, playing the game, playing like the others,

19
00:01:14,133 --> 00:01:17,810
but with another vision of the world, which is not "we have to make money",

20
00:01:17,848 --> 00:01:19,840
We really don't care to make money.

21
00:01:20,200 --> 00:01:25,257
If we have to sell the subscription at 10€, 50€ or 100€

22
00:01:25,520 --> 00:01:27,200
then we will sell it to 10, 50 or 100.

23
00:01:27,200 --> 00:01:31,440
At 50€ it will be more difficult than at 10, of course,

24
00:01:31,820 --> 00:01:33,937
but this is not the issue.

25
00:01:34,080 --> 00:01:35,930
We are associations of volonteer.

26
00:01:36,000 --> 00:01:38,371
Our first goal is not to make money

27
00:01:38,371 --> 00:01:41,250
to grow and feed stake holders.

28
00:01:41,250 --> 00:01:41,955
this is not the subject.

29
00:01:42,026 --> 00:01:45,320
The subject of our associations is to defend Internet neutrality

30
00:01:45,450 --> 00:01:47,671
and to promote decentralized networks

31
00:01:47,742 --> 00:01:48,520
this is not the same.

32
00:01:48,660 --> 00:01:50,542
All the preceding make arriving

33
00:01:50,540 --> 00:01:53,431
on the market, and in discussion with regulator

34
00:01:53,573 --> 00:01:56,770
some actors who do not have a narrow view but a large one.

35
00:01:57,000 --> 00:01:59,571
So this make come in the regulator office

36
00:01:59,620 --> 00:02:01,942
people like me who has study philosophy

37
00:02:02,000 --> 00:02:03,591
who explain them the philosophy...

38
00:02:03,662 --> 00:02:05,450
to talk about telecom.

39
00:02:05,450 --> 00:02:06,826
With this point of view,

40
00:02:06,960 --> 00:02:09,866
with this very strange manner to link things together

41
00:02:10,140 --> 00:02:12,257
we arrive to an analysis view angle that is larger,

42
00:02:12,285 --> 00:02:15,280
and then we touch society issues

43
00:02:15,367 --> 00:02:16,420
that ARCEP does not see

44
00:02:16,426 --> 00:02:18,000
because it does not have the right tools.

45
00:02:18,000 --> 00:02:20,657
When economic actors are making war at each other,

46
00:02:20,906 --> 00:02:23,457
what are the collateral damage for people? 

47
00:02:23,600 --> 00:02:25,980
what are the collateral damage for society?

48
00:02:26,180 --> 00:02:28,320
In what means does it distort society?

49
00:02:28,320 --> 00:02:31,000
Google that fight against Facebook

50
00:02:31,200 --> 00:02:35,320
to know who will have the biggest market share in advertising,

51
00:02:35,320 --> 00:02:36,942
this is an economic question,

52
00:02:36,971 --> 00:02:40,742
and this has huge consequences on people privacy,

53
00:02:40,840 --> 00:02:43,020
on the way the society structure itself

54
00:02:43,320 --> 00:02:47,520
on the way hatred goes up or not inside society

55
00:02:47,820 --> 00:02:50,520
These are fraught with consequences.

56
00:02:50,820 --> 00:02:52,981
We have a very precise place on this market

57
00:02:53,076 --> 00:02:54,980
we are the onlyones to have both feet :

58
00:02:55,020 --> 00:02:58,600
We have one foot in the market, because we are operator so we have this activity.

59
00:02:58,650 --> 00:03:02,000
We are experts on the question "What is an operator?"

60
00:03:02,057 --> 00:03:04,970
We have operator that date from before Orange arrive.

61
00:03:05,000 --> 00:03:07,314
We have FDN which is 1992.

62
00:03:07,340 --> 00:03:09,714
And we have also a foot completely outside

63
00:03:09,740 --> 00:03:12,000
because we are people from the "civil society".

64
00:03:12,050 --> 00:03:12,906
We are volonteer.

65
00:03:12,960 --> 00:03:16,906
We have all another job beside being an operator,

66
00:03:17,310 --> 00:03:19,484
and for some the other job has nothing to do ;

67
00:03:19,590 --> 00:03:21,537
this create an open mind,

68
00:03:21,640 --> 00:03:24,400
a large sight

69
00:03:24,880 --> 00:03:26,385
that enable to help the regulator

70
00:03:26,458 --> 00:03:30,785
to assume this mission to defend Internet neutrality

71
00:03:30,850 --> 00:03:32,400
for which it is not armed.

72
00:03:32,400 --> 00:03:34,640
We are some kind of regulator sword arm, but not payed for.

73
00:03:34,680 --> 00:03:38,440
In part because we have this general interest view that even ARCEP does not have.

74
00:03:38,514 --> 00:03:40,420
ARCEP does not have general interest view.

75
00:03:40,465 --> 00:03:43,770
We have it, because we start first from final user to come on the market,

76
00:03:43,820 --> 00:03:47,342
whereas ARCEP start from the market to come, maybe, to the final user.

77
00:03:47,342 --> 00:03:51,200
We often prove that one of the movement works better than the other one.

